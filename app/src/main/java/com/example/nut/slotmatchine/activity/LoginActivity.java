package com.example.nut.slotmatchine.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.nut.slotmatchine.R;
import com.example.nut.slotmatchine.databinding.ActivityLoginBinding;
import com.example.nut.slotmatchine.fragment.LoginFragment;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;


public class LoginActivity extends BaseActivity implements LoginFragment.FragmentLoginListener, GoogleApiClient.OnConnectionFailedListener {
    private String TAG = "TAG";
    private static final int RC_SIGN_IN = 9001;

    ActivityLoginBinding binding;

    public FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        initInstance();

        if (savedInstanceState == null) {

            mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {
                        // User is signed in
                        Log.d(TAG, "From LoginActivity onAuthStateChanged:signed_in:" + user.getUid());

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.face_in, R.anim.face_out);
                        finish();
                    } else {
                        // User is signed out
                        Log.d(TAG, "From LoginActivity onAuthStateChanged:signed_out");
                        getSupportFragmentManager().beginTransaction()
                                .add(R.id.contentContainer, LoginFragment.newInstance())
                                .commit();
                    }
                }
            };

        }
    }

    private void initInstance() {
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onBtnClockSignInWithPassword(String email, String password) {
        progressDialog.showProgressDialog(getString(R.string.progress_dialog_loading), getString(R.string.progress_dialog_authenticating_with_firebase));
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "From Login signInWithEmail:onComplete:" + task.isSuccessful());

                        if (!task.isSuccessful()) {

                            Log.w(TAG, "From Login signInWithEmail:failed", task.getException());
                            toast.showToast("The password is invalid or the user does not have a password");

                        } else {
                            toast.showToast("Login with Email complete");
                        }
                        progressDialog.hideProgressDialog();
                    }
                });
    }

    @Override
    public void onBtnClickSignInWithGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed, update UI appropriately
                toast.showToast("Google Sign In failed");
            }
        }
    }

    // sign in with google
    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        progressDialog.showProgressDialog(getString(R.string.progress_dialog_loading), getString(R.string.progress_dialog_authenticating_with_firebase));

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        writeUserToDatabase(task.getResult().getUser());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                        }
                        progressDialog.hideProgressDialog();
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        toast.showToast("Google Play Services error.");
    }
}
