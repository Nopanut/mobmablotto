package com.example.nut.slotmatchine.fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nut.slotmatchine.R;
import com.example.nut.slotmatchine.databinding.FragmentNumber3Binding;
import com.example.nut.slotmatchine.util.CheckNumber;
import com.example.nut.slotmatchine.util.ToastDialog;
import com.example.nut.slotmatchine.view.ListLottery;
import com.example.nut.slotmatchine.view.CustomViewGroupNumber3;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@SuppressWarnings("unused")
public class FragmentNumber3 extends Fragment implements View.OnClickListener {

    FragmentNumber3Binding binding;
    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    private String userName, userId, providerId, numberSave = "";
    private int countNumber;

    CheckNumber checkNum;
    ToastDialog toast;
    FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListener;

    public FragmentNumber3() {
        super();
    }

    @SuppressWarnings("unused")
    public static FragmentNumber3 newInstance() {
        FragmentNumber3 fragment = new FragmentNumber3();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_number3, container, false);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
        toast = new ToastDialog(getContext());
        checkNum = new CheckNumber(getContext());

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    userId = user.getUid();
                    userName = user.getDisplayName();
                    updateUI(user);
                    Log.d("num3", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    Log.d("num3", "onAuthStateChanged:signed_out");
                }
            }
        };
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here
        countNumber = 1;

        binding.btnSent.setOnClickListener(this);
        binding.btnReset.setOnClickListener(this);
        binding.btnAddNumber.setOnClickListener(this);
        binding.btnMinusNumber.setOnClickListener(this);

    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    //     Save Instance State Here
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    //     Restore Instance State Here
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }

    @Override
    public void onClick(View v) {
        if (v == binding.btnSent) {
            onClickButtonSent();

        } else if (v == binding.btnReset) {
            onClickButtonReset();

        } else if (v == binding.btnAddNumber) {
            onClickButtonAddNumber();

        } else if ((v == binding.btnMinusNumber)) {
            onClickButtonMinusNumber();

        }
    }

    private void onClickButtonSent() {
        switch (countNumber) {
            case 1:
                if (checkText(binding.viewGroup1)) {
                    uploadNumber(binding.viewGroup1);

                    toast.showToast("บันทึกเลข " + numberSave.trim());
                    buttonReset();
                }
                break;
            case 2:
                if (checkText(binding.viewGroup1) && checkText(binding.viewGroup2)
                        && binding.viewGroup2.getVisibility() == View.VISIBLE) {
                    uploadNumber(binding.viewGroup1);
                    uploadNumber(binding.viewGroup2);

                    toast.showToast("บันทึกเลข " + numberSave.trim());
                    buttonReset();
                }
                break;
            case 3:
                if (checkText(binding.viewGroup1) && checkText(binding.viewGroup2)
                        && checkText(binding.viewGroup3) && binding.viewGroup3.getVisibility() == View.VISIBLE) {
                    uploadNumber(binding.viewGroup1);
                    uploadNumber(binding.viewGroup2);
                    uploadNumber(binding.viewGroup3);

                    toast.showToast("บันทึกเลข " + numberSave.trim());
                    buttonReset();
                }
                break;
            case 4:
                if (checkText(binding.viewGroup1) && checkText(binding.viewGroup2)
                        && checkText(binding.viewGroup3) && checkText(binding.viewGroup4)
                        && binding.viewGroup2.getVisibility() == View.VISIBLE) {
                    uploadNumber(binding.viewGroup1);
                    uploadNumber(binding.viewGroup2);
                    uploadNumber(binding.viewGroup3);
                    uploadNumber(binding.viewGroup4);

                    toast.showToast("บันทึกเลข " + numberSave.trim());
                    buttonReset();
                }
                break;
            default:
                break;
        }
    }

    private void onClickButtonReset() {
        countNumber = 1;
        binding.viewGroup2.setVisibility(View.GONE);
        binding.viewGroup3.setVisibility(View.GONE);
        binding.viewGroup4.setVisibility(View.GONE);
        binding.btnAddNumber.setVisibility(View.VISIBLE);
        binding.btnMinusNumber.setVisibility(View.INVISIBLE);
        buttonReset();
    }

    private void onClickButtonAddNumber() {
        countNumber++;
        switch (countNumber) {
            case 2:
                binding.viewGroup2.setVisibility(View.VISIBLE);
                binding.btnMinusNumber.setVisibility(View.VISIBLE);
                break;
            case 3:
                binding.viewGroup3.setVisibility(View.VISIBLE);
                break;
            case 4:
                binding.viewGroup4.setVisibility(View.VISIBLE);
                binding.btnMinusNumber.setVisibility(View.VISIBLE);
                binding.btnAddNumber.setVisibility(View.INVISIBLE);
                break;
            default:
                break;
        }
    }

    private void onClickButtonMinusNumber() {
        countNumber--;
        switch (countNumber) {
            case 1:
                binding.viewGroup2.setVisibility(View.GONE);
                binding.btnMinusNumber.setVisibility(View.INVISIBLE);
                break;
            case 2:
                binding.viewGroup3.setVisibility(View.GONE);
                break;
            case 3:
                binding.viewGroup4.setVisibility(View.GONE);
                binding.btnAddNumber.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    private boolean checkText(CustomViewGroupNumber3 viewGroup) {
        boolean pass;
        if (!viewGroup.getLotteryNumber().equals("")
                && !viewGroup.getLotteryValue1().equals("") && !viewGroup.getLotteryValue2().equals("")) {
            if (checkNum.CountNumber(viewGroup.getLotteryNumber()) == 3) {
                pass = true;

            } else {
                toast.showToast("กรุณากรอกเลขที่จะซื้อ 3 หลัก");
                pass = false;
            }

        } else {
            toast.showToast("กรุณากรอกตัวเลข 0 ในช่องที่ไม่ซื้อ");
            pass = false;
        }
        return pass;
    }

    private void uploadNumber(CustomViewGroupNumber3 viewGroup) {

        //String key = mRootRef.child("Number2").child(binding.userName.getText().toString()).push().getKey();

        DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");
        String date = df.format(Calendar.getInstance().getTime());

        ListLottery listLottery = new ListLottery(viewGroup.getLotteryNumber()
                , viewGroup.getLotteryValue1()
                , viewGroup.getLotteryValue2()
                , date);

        mRootRef.child("user-number").child(userId).child("Number3").push()
                .setValue(listLottery);

        DatabaseReference mPaymentRef = mRootRef.child("user-payment").child(userName);
        mPaymentRef.setValue("not confirm");

        numberSave += viewGroup.getLotteryNumber() + " ";
    }

    private void buttonReset() {
        binding.viewGroup1.resetNumber();
        binding.viewGroup2.resetNumber();
        binding.viewGroup3.resetNumber();
        binding.viewGroup4.resetNumber();
        numberSave = "";
    }

    private void updateUI(FirebaseUser user) {
        for (UserInfo profile : user.getProviderData()) {
            // Id of the provider (ex: google.com)
            if (!profile.getProviderId().equals("firebase")) {
                providerId = profile.getProviderId();
                Log.d("num3", "getProviderId:" + providerId);
            }
        }
        if (providerId.equals("password")) {

            DatabaseReference mNameRef = mRootRef.child("users").child(user.getUid()).child("username");
            mNameRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String value = dataSnapshot.getValue(String.class);
                    binding.userName.setText(value);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });

            binding.userPic.getLayoutParams().width = (getResources().getDisplayMetrics().widthPixels / 100) * 24;
            binding.userPic.setImageResource(R.drawable.ic_face_black_48dp);

        } else {
            if (user.getPhotoUrl() != null) {
                new DownloadImageTask().execute(user.getPhotoUrl().toString());
            }
            binding.userName.setText(user.getDisplayName());
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... urls) {
            Bitmap mIcon = null;
            try {
                InputStream in = new URL(urls[0]).openStream();
                mIcon = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                binding.userPic.getLayoutParams().width = (getResources().getDisplayMetrics().widthPixels / 100) * 24;
                binding.userPic.setImageBitmap(result);
            }
        }
    }
}
