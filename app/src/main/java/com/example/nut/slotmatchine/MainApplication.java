package com.example.nut.slotmatchine;

//import com.facebook.FacebookSdk;

import android.app.Application;

import com.example.nut.slotmatchine.manager.Contextor;
//import com.facebook.appevents.AppEventsLogger;


/**
 * Created by Nut on 6/28/2016.
 */
public class MainApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        Contextor.getInstance().init(getApplicationContext());
        // Initialize the SDK before executing any other operations,
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
