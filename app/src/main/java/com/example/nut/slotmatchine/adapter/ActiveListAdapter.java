package com.example.nut.slotmatchine.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.nut.slotmatchine.R;
import com.example.nut.slotmatchine.view.ListLottery;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.database.Query;

/**
 * Created by Nut on 7/3/2016.
 */
public class ActiveListAdapter extends FirebaseListAdapter<ListLottery> {

    public ActiveListAdapter(Activity activity, Class<ListLottery> modelClass, int modelLayout, Query ref) {
        super(activity, modelClass, modelLayout, ref);
        this.mActivity = activity;
    }

    @Override
    protected void populateView(View v, ListLottery model, final int position) {

        String number = model.getNumber() + " : " + model.getValue1() + " X " + model.getValue2();
        ((TextView) v.findViewById(R.id.tvDatabaseNumber)).setText(number);
        ((TextView) v.findViewById(R.id.tvDatabaseDate)).setText(model.getDate());

        v.findViewById(R.id.btnListDel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRef(position).removeValue();
//                notifyDataSetChanged();
            }
        });
    }
}
