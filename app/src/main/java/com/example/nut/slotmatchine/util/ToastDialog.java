package com.example.nut.slotmatchine.util;

import android.content.Context;
import android.os.Debug;
import android.util.Log;
import android.widget.Toast;

import com.example.nut.slotmatchine.R;

/**
 * Created by Nut on 6/29/2016.
 */
public class ToastDialog {
    private Context context = null;
    Toast toast = null;

    public ToastDialog(Context context) {
        this.context = context;
    }

    public void showToast(String string) {
        try {
            toast.getView().isShown();
            toast.setText(string);

        } catch (Exception e) {
            toast = Toast.makeText(context, string, Toast.LENGTH_SHORT);
        }
        toast.show();

    }
}
