package com.example.nut.slotmatchine.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nut.slotmatchine.R;
import com.example.nut.slotmatchine.activity.HistoryActivity;
import com.example.nut.slotmatchine.activity.OrderActivity;
import com.example.nut.slotmatchine.activity.PaymentActivity;
import com.example.nut.slotmatchine.databinding.FragmentMainBinding;
import com.example.nut.slotmatchine.util.ToastDialog;

public class MainFragment extends Fragment implements View.OnClickListener {
    FragmentMainBinding binding;
    final private int MY_PERMISSIONS_REQUEST_CODE = 123;

    public MainFragment() {
        super();
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
        View rootView = binding.getRoot();
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here

        checkPermissions();

        binding.btnBuyNum2.setOnClickListener(this);
        binding.btnBuyNum3.setOnClickListener(this);
        binding.btnRecord.setOnClickListener(this);
        binding.btnPay.setOnClickListener(this);
        binding.btnProfile.setOnClickListener(this);
        binding.btnHelp.setOnClickListener(this);

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
    * Save Instance State Here
    */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }

    @Override
    public void onClick(View v) {

        if (v == binding.btnBuyNum2) {
            Intent i = new Intent(getContext(), OrderActivity.class);
            i.putExtra("number", "number2");
            startActivity(i);

        } else if (v == binding.btnBuyNum3) {
            Intent i = new Intent(getContext(), OrderActivity.class);
            i.putExtra("number", "number3");
            startActivity(i);

        } else if (v == binding.btnRecord) {

            Intent i = new Intent(getContext(), HistoryActivity.class);
            startActivity(i);
        } else if (v == binding.btnPay) {

            Intent i = new Intent(getContext(), PaymentActivity.class);
            startActivity(i);

        } else if (v == binding.btnProfile) {
            ToastDialog toast = new ToastDialog(getContext());
            toast.showToast("coming soon");
        } else if (v == binding.btnHelp) {
            try {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:0859261255"));
                startActivity(callIntent);
            } catch (android.content.ActivityNotFoundException e) {


                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(this).attach(this).commit();
            }
        }
    }

    private void checkPermissions() {

        int permissionCheckCall = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE);
        int permissionCheckStorage = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionCheckStorage != PackageManager.PERMISSION_GRANTED && permissionCheckCall != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_CODE);
                return;
                // MY_PERMISSIONS_REQUEST_ACCESS_CALL_NUMBER is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }
}
