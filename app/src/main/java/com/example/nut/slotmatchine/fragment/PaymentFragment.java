package com.example.nut.slotmatchine.fragment;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.example.nut.slotmatchine.R;
import com.example.nut.slotmatchine.databinding.FragmentPaymentBinding;
import com.example.nut.slotmatchine.util.ProgressDialogBox;
import com.example.nut.slotmatchine.util.ToastDialog;
import com.example.nut.slotmatchine.view.ListLottery;
import com.example.nut.slotmatchine.view.state.BundleSavedState;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


@SuppressWarnings("unused")
public class PaymentFragment extends Fragment implements View.OnClickListener {

    private static int RESULT_LOAD_IMAGE = 111;

    FragmentPaymentBinding binding;

    ToastDialog toast;
    ProgressDialogBox progressDialog;
    private int totalValue;
    private String userName, providerId, paymentType = "";
    private Uri selectedImage;
    private Bitmap yourSelectedImage;
    private BundleSavedState bundleSavedState;
    private boolean setData;

    FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListener;

    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    FirebaseStorage storage;
    StorageReference storageRef;
    UploadTask uploadTask;

    public PaymentFragment() {
        super();
    }

    @SuppressWarnings("unused")
    public static PaymentFragment newInstance() {
        PaymentFragment fragment = new PaymentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment, container, false);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
        toast = new ToastDialog(getContext());
        progressDialog = new ProgressDialogBox(getContext());
        setData = false;

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    updateUI(user);
                    updateListData(user, "Number2");
                    updateListData(user, "Number3");
                    setData = true;
                    Log.d("pic", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    Log.d("pic", "onAuthStateChanged:signed_out");
                }
            }
        };

        // Create a storage reference from our app
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://project-1160081552739098939.appspot.com/");

    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here

        if (yourSelectedImage != null) {
            onRestoreInstanceState(savedInstanceState);
        }


        binding.btnAddPic.setOnClickListener(this);
        binding.btnSentPic.setOnClickListener(this);
        binding.IvPayment.setOnClickListener(this);
        binding.rgOperator.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                setupButtonPayment(checkedId);
            }
        });
    }

    private void setupButtonPayment(int id) {
        switch (id) {
            case R.id.rdBtn1:
                binding.btnAddPic.setVisibility(View.VISIBLE);
                break;
            case R.id.rdBtn2:
                binding.btnAddPic.setVisibility(View.INVISIBLE);
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        if (v == binding.btnAddPic) {
            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, RESULT_LOAD_IMAGE);

        } else if (v == binding.btnSentPic) {
            if (paymentType.equals("not confirm")) {
                if (binding.rgOperator.getCheckedRadioButtonId() == R.id.rdBtn1) {
                    if (selectedImage != null) {
                        uploadImage(selectedImage);
                        paymentType = "confirm";
                        confirmUpload();

                    } else {
                        toast.showToast("เพิ่มรูปภาพใบเสร็จก่อนกดส่ง");
                    }
                } else {
                    paymentType = "pay later";
                    confirmUpload();
                }
            } else
                toast.showToast("ทำรายการแล้ว");

        } else if (v == binding.IvPayment) {
            Bundle b = new Bundle();
            b.putParcelable("image", yourSelectedImage);

            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.from_right, R.anim.to_left, R.anim.from_left, R.anim.to_right)
                    .replace(R.id.contentContainer, PictureInfoFragment.newInstance(b))
                    .addToBackStack(null)
                    .commit();
        }
    }

    private void updateListData(FirebaseUser user, String number) {
        if (!setData) {

            DatabaseReference mRootList = mRootRef.child("user-number").child(user.getUid()).child(number);

            mRootList.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.e("Count ", "" + dataSnapshot.getChildrenCount());
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        ListLottery data = postSnapshot.getValue(ListLottery.class);
                        if (data.getNumber() != null) {
                            updateNumber(data);
                        }
                    }
                    if (dataSnapshot.getValue() != null) {
                        updateTotalValue();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void updateNumber(ListLottery data) {
        binding.tvTotalNumber.append(data.getNumber() + " : " + data.getValue1() + " x " + data.getValue2() + "\n");
        totalValue += parseNumber(data.getValue1()) + parseNumber(data.getValue2());
        Log.d("plus", "totalValue = " + totalValue
                + "(" + parseNumber(data.getValue1()) + " + " + parseNumber(data.getValue2()) + ")");
    }

    private void updateTotalValue() {
        DatabaseReference mPaymentRef = mRootRef.child("user-payment").child(userName);
        mPaymentRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(String.class).equals("confirm")) {
                    paymentType = "confirm";
                    binding.tvTotalValue.setText("รวม = " + totalValue + " (จ่ายแล้ว)");
                } else if (dataSnapshot.getValue(String.class).equals("pay later")) {
                    paymentType = "pay later";
                    binding.tvTotalValue.setText("รวม = " + totalValue + " (จ่ายทีหลัง)");
                } else {
                    paymentType = "not confirm";
                    binding.tvTotalValue.setText("รวม = " + totalValue);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                binding.tvTotalValue.setText("รวม = " + totalValue);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            binding.tvTotalNumber.setText("");
            totalValue = 0;
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here

    }

    /*
     * Restore Instance State Here
     */
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here

        binding.IvPayment.setImageBitmap(Bitmap.createScaledBitmap(yourSelectedImage,
                yourSelectedImage.getWidth(), yourSelectedImage.getHeight(), false));
        binding.IvPayment.setVisibility(View.VISIBLE);

    }

    private int parseNumber(String text) {
        int number = 0;
        try {
            if (text != null) {
                number = Integer.parseInt(text);
            }
        } catch (NumberFormatException e) {

        }
        return number;
    }

    private void updateUI(FirebaseUser user) {
        for (UserInfo profile : user.getProviderData()) {
            // Id of the provider (ex: google.com)
            if (!profile.getProviderId().equals("firebase")) {
                providerId = profile.getProviderId();
                Log.d("num3", "getProviderId:" + providerId);
            }
        }
        if (providerId.equals("password")) {

            DatabaseReference mNameRef = mRootRef.child("users").child(user.getUid()).child("username");
            mNameRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    userName = dataSnapshot.getValue(String.class);
                    binding.userName.setText(userName);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });

            binding.userPic.getLayoutParams().width = (getResources().getDisplayMetrics().widthPixels / 100) * 24;
            binding.userPic.setImageResource(R.drawable.ic_face_black_48dp);

        } else {
            if (user.getPhotoUrl() != null) {
                new DownloadImageTask().execute(user.getPhotoUrl().toString());
            }
            userName = user.getDisplayName();
            binding.userName.setText(userName);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && intent != null) {
            selectedImage = intent.getData();

            InputStream imageStream = null;

            try {
                imageStream = getContext().getContentResolver().openInputStream(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            yourSelectedImage = BitmapFactory.decodeStream(imageStream);
            binding.IvPayment.setImageBitmap(Bitmap.createScaledBitmap(yourSelectedImage,
                    yourSelectedImage.getWidth(), yourSelectedImage.getHeight(), false));
            binding.IvPayment.setVisibility(View.VISIBLE);
        }
    }

    private void uploadImage(Uri selectedImage) {
        // Create the file metadata
        StorageMetadata metadata = new StorageMetadata.Builder()
                .setContentType("image/jpeg")
                .build();

        UploadTask uploadTask = storageRef.child("images/" + userName + "/" + getDate("yyMMddHHmmss") + selectedImage.getLastPathSegment())
                .putFile(selectedImage, metadata);

        progressDialog.showProgressDialog("Uploading", "OnProgress");

        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                System.out.println("Upload is " + progress + "% done");
            }
        }).addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {
                System.out.println("Upload is paused");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Handle unsuccessful uploads

            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                progressDialog.hideProgressDialog();
                toast.showToast("upload success");
                onStart();
            }
        });

    }

    private void confirmUpload() {
        DatabaseReference mPaymentRef = mRootRef.child("user-payment").child(userName);

        mPaymentRef.setValue(paymentType);

    }


    private String getDate(String dateFormat) {
        DateFormat df = new SimpleDateFormat(dateFormat);
        String date = df.format(new java.util.Date());

        return date;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... urls) {
            Bitmap mIcon = null;
            try {
                InputStream in = new URL(urls[0]).openStream();
                mIcon = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                binding.userPic.getLayoutParams().width = (getResources().getDisplayMetrics().widthPixels / 100) * 24;
                binding.userPic.setImageBitmap(result);
            }
        }
    }
}
