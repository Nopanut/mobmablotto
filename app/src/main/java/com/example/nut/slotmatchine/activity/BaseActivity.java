package com.example.nut.slotmatchine.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.nut.slotmatchine.R;
import com.example.nut.slotmatchine.util.ProgressDialogBox;
import com.example.nut.slotmatchine.util.ToastDialog;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Nut on 8/2/2016.
 */
public class BaseActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    protected GoogleApiClient mGoogleApiClient;

    public FirebaseAuth mAuth;
    public DatabaseReference mRootRef;

    private Boolean exit = false;
    ToastDialog toast;
    ProgressDialogBox progressDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toast = new ToastDialog(this);
        progressDialog = new ProgressDialogBox(this);

        // get root of database
        mRootRef = FirebaseDatabase.getInstance().getReference();


        mAuth = FirebaseAuth.getInstance();

         /* Setup the Google API object to allow Google logins */
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        /**
         * Build a GoogleApiClient with access to the Google Sign-In API and the
         * options specified by gso.
         */
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    // store to firebase database
    public void writeUserToDatabase(FirebaseUser user) {

        if (user != null) {
            if (user.getPhotoUrl() != null) {
                mRootRef.child("users").child(user.getUid()).child("profile_picture")
                        .setValue(user.getPhotoUrl().getSchemeSpecificPart());
            }
            mRootRef.child("users").child(user.getUid()).child("username")
                    .setValue(user.getDisplayName());
            mRootRef.child("users").child(user.getUid()).child("email")
                    .setValue(user.getEmail());
            mRootRef.child("users").child(user.getUid()).child("uid")
                    .setValue(user.getUid());

        }
        progressDialog.hideProgressDialog();
    }


    @Override
    public void onStop() {
        super.onStop();

        progressDialog.hideProgressDialog();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void googleSignOut() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage(R.string.logout);
        alert.setCancelable(false);
        alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Firebase sign out
                mAuth.signOut();
                // Google sign out
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(@NonNull Status status) {
                                ToastDialog toast = new ToastDialog(getApplicationContext());
                                toast.showToast("sign out");
                            }
                        }
                );
            }
        });
        alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alert.show();
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            if (exit) {
                super.onBackPressed();
            } else {
                toast.showToast("Press Back again to Exit.");
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 2 * 1000);
            }
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }
}
