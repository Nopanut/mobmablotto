package com.example.nut.slotmatchine.util;

import android.content.Context;
import android.app.ProgressDialog;

/**
 * Created by Nut on 6/29/2016.
 */
public class ProgressDialogBox {
    private ProgressDialog mProgressDialog;

    private Context context = null;

    public ProgressDialogBox(Context context) {
        this.context = context;
    }


    public void showProgressDialog(String title, String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new android.app.ProgressDialog(context);
            mProgressDialog.setTitle(title);
            mProgressDialog.setMessage(message);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
        }
        mProgressDialog.show();
    }

    public void showProgressDialogWithBar(String title, String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new android.app.ProgressDialog(context);
            mProgressDialog.setTitle(title);
            mProgressDialog.setMessage(message);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setProgress(0);
            mProgressDialog.setMax(100);
        }
        mProgressDialog.show();
    }

    public void updateProgressDialog(int status) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.setProgress(status);
        }
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
