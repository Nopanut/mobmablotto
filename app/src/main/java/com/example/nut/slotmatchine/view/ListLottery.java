package com.example.nut.slotmatchine.view;

public class ListLottery {
    String number;
    String value1;
    String value2;
    String date;

    public ListLottery() {

    }

    public ListLottery(String number, String value1, String value2, String date) {
        this.number = number;
        this.value1 = value1;
        this.value2 = value2;
        this.date = date;
    }

    public String getNumber() {
        return number;
    }

    public String getValue1() {
        return value1;
    }

    public String getValue2() {
        return value2;
    }

    public String getDate() {
        return date;
    }
}
