package com.example.nut.slotmatchine.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.nut.slotmatchine.R;
import com.google.android.gms.common.SignInButton;

public class LoginFragment extends Fragment implements View.OnClickListener {

    private EditText mEditTextEmailInput, mEditTextPasswordInput;

    FragmentLoginListener mCallback;

    public interface FragmentLoginListener {
        void onBtnClickSignInWithGoogle();
        void onBtnClockSignInWithPassword(String email, String password);
    }

    public LoginFragment() {
        super();
    }

    @SuppressWarnings("unused")
    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);


        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (FragmentLoginListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here

        rootView.findViewById(R.id.tv_sign_up).setOnClickListener(this);
        rootView.findViewById(R.id.login_with_password).setOnClickListener(this);

        mEditTextEmailInput = (EditText) rootView.findViewById(R.id.edit_text_email);
        mEditTextPasswordInput = (EditText) rootView.findViewById(R.id.edit_text_password);
        SignInButton signInGoogleButton = (SignInButton) rootView.findViewById(R.id.login_with_google);
        signInGoogleButton.setSize(SignInButton.SIZE_WIDE);
        signInGoogleButton.setOnClickListener(this);

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_sign_up:
                CreateAccountPage();
                Log.d("TAG", "From Login go to CrateAccount");
                break;
            case R.id.login_with_password:
                signInWithPassword(mEditTextEmailInput.getText().toString(), mEditTextPasswordInput.getText().toString());
                break;
            case R.id.login_with_google:
                signInWithGoogle();
                break;
        }
    }

    private void signInWithGoogle() {
        mCallback.onBtnClickSignInWithGoogle();
    }


    // sign in with email and pass
    private void signInWithPassword(String email, String password) {
        if (!validateForm()) {
            return;
        }
        mCallback.onBtnClockSignInWithPassword(email, password);

    }


    // Check edit text
    private boolean validateForm() {
        if (TextUtils.isEmpty(mEditTextEmailInput.getText().toString())) {
            mEditTextEmailInput.setError("Required.");
            return false;
        } else if (TextUtils.isEmpty(mEditTextPasswordInput.getText().toString())) {
            mEditTextPasswordInput.setError("Required.");
            return false;
        } else {
            mEditTextEmailInput.setError(null);
            return true;
        }
    }


    /**
     * Open CreateAccountFragment when user taps on "Sign up" TextView
     */
    private void CreateAccountPage() {

        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.from_right, R.anim.to_left, R.anim.from_left, R.anim.to_right)
                .replace(R.id.contentContainer, FragmentCreateAccount.newInstance())
                .addToBackStack(null)
                .commit();
    }




//    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
//        @Override
//        protected Bitmap doInBackground(String... urls) {
//            Bitmap mIcon = null;
//            try {
//                InputStream in = new URL(urls[0]).openStream();
//                mIcon = BitmapFactory.decodeStream(in);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return mIcon;
//        }
//
//        @Override
//        protected void onPostExecute(Bitmap result) {
//            if (result != null) {
//                mImageView.getLayoutParams().width = (getResources().getDisplayMetrics().widthPixels / 100) * 24;
//                mImageView.setImageBitmap(result);
//            }
//        }
//    }
}
