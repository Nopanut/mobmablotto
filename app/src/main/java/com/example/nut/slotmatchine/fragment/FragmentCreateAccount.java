package com.example.nut.slotmatchine.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.nut.slotmatchine.R;
import com.example.nut.slotmatchine.util.ProgressDialogBox;
import com.example.nut.slotmatchine.util.ToastDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


@SuppressWarnings("unused")
public class FragmentCreateAccount extends Fragment implements View.OnClickListener {

    private static final String TAG = "FragmentCreateAccount";

    private EditText mEditTextUsernameCreate, mEditTextEmailCreate, mEditTextPasswordCreate;

    private ProgressDialogBox mAuthProgressDialog;
    private FirebaseAuth mAuth;
    private DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();

    private ToastDialog toast;

    public FragmentCreateAccount() {
        super();
    }

    @SuppressWarnings("unused")
    public static FragmentCreateAccount newInstance() {
        FragmentCreateAccount fragment = new FragmentCreateAccount();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_create_account, container, false);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
        toast = new ToastDialog(getContext());
        mAuthProgressDialog = new ProgressDialogBox(getContext());
        mAuth = FirebaseAuth.getInstance();

    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here
        mEditTextUsernameCreate = (EditText) rootView.findViewById(R.id.edit_text_username_create);
        mEditTextEmailCreate = (EditText) rootView.findViewById(R.id.edit_text_email_create);
        mEditTextPasswordCreate = (EditText) rootView.findViewById(R.id.edit_text_password_create);

        rootView.findViewById(R.id.tv_sign_in).setOnClickListener(this);
        rootView.findViewById(R.id.btn_create_account_final).setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_create_account_final:
                createAccount(mEditTextEmailCreate.getText().toString(), mEditTextPasswordCreate.getText().toString());
                Log.d("TAG", "From Login go to CrateAccount");
                break;
            case R.id.tv_sign_in:
                LoginPage();
                break;
            default:
                break;
        }
    }

    private void createAccount(String email, String password) {
        if (!validateForm()) {
            return;
        }
        mAuthProgressDialog.showProgressDialog(getResources().getString(R.string.progress_dialog_loading), getResources().getString(R.string.progress_dialog_creating_user_with_firebase));

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener((Activity) getContext(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "From FragmentCrateAccount signInWithEmail:failed", task.getException());
                            toast.showToast("สร้างบัญชี " + mEditTextUsernameCreate.getText().toString() + " ไม่สำเร็จ");

                        } else {
                            Log.d(TAG, "From FragmentCrateAccount signInWithEmail:onComplete:" + task.isSuccessful());
                            toast.showToast("สร้างบัญชี " + mEditTextUsernameCreate.getText().toString() + " เรียบร้อยแล้ว");

                            writeUserData(task.getResult().getUser());

                            mAuth.signOut();
                        }
                        mAuthProgressDialog.hideProgressDialog();
                    }
                });
    }


    private void writeUserData(FirebaseUser user) {
        if (user != null) {
            mRootRef.child("users").child(user.getUid()).child("username")
                    .setValue(mEditTextUsernameCreate.getText().toString().trim());
            mRootRef.child("users").child(user.getUid()).child("email")
                    .setValue(user.getEmail());
            mRootRef.child("users").child(user.getUid()).child("profile_picture")
                    .setValue(null);
            mRootRef.child("users").child(user.getUid()).child("uid")
                    .setValue(user.getUid());

        }
    }

    private boolean validateForm() {
        if (TextUtils.isEmpty(mEditTextUsernameCreate.getText().toString())) {
            mEditTextUsernameCreate.setError("Required.");
            return false;
        } else if (TextUtils.isEmpty(mEditTextEmailCreate.getText().toString())) {
            mEditTextEmailCreate.setError("Required.");
            return false;
        } else if (TextUtils.isEmpty(mEditTextPasswordCreate.getText().toString())) {
            mEditTextPasswordCreate.setError("Required.");
            return false;
        } else {
            mEditTextEmailCreate.setError(null);
            return true;
        }
    }

    private void LoginPage() {
        getFragmentManager().popBackStack();
    }
}
