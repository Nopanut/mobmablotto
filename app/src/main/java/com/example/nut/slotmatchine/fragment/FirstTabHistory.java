package com.example.nut.slotmatchine.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.nut.slotmatchine.R;
import com.example.nut.slotmatchine.adapter.ActiveListAdapter;
import com.example.nut.slotmatchine.databinding.FragmentTabFirstBinding;
import com.example.nut.slotmatchine.util.ToastDialog;
import com.example.nut.slotmatchine.view.ListLottery;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


@SuppressWarnings("unused")
public class FirstTabHistory extends Fragment {

    FragmentTabFirstBinding binding;
    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    private ActiveListAdapter adapter;
    private String userId;

    public FirstTabHistory() {
        super();
    }

    @SuppressWarnings("unused")
    public static FirstTabHistory newInstance(String userId) {
        FirstTabHistory fragment = new FirstTabHistory();
        Bundle args = new Bundle();
        args.putString("userId", userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tab_first, container, false);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    private void init(Bundle savedInstanceState) {
        // Init Fragment level's variable(s) here
        userId = getArguments().getString("userId");
        Log.d("userId", userId);
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        // Init 'View' instance(s) with rootView.findViewById here

        DatabaseReference mRootList = mRootRef.child("user-number").child(userId).child("Number2");
        adapter = new ActiveListAdapter(getActivity(), ListLottery.class, R.layout.list_view_lottery, mRootList);

        binding.listView.setAdapter(adapter);
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        adapter.cleanup();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance State here
    }

}
