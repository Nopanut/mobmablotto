package com.example.nut.slotmatchine.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.nut.slotmatchine.R;
import com.example.nut.slotmatchine.databinding.ActivityOrderBinding;
import com.example.nut.slotmatchine.fragment.FragmentNumber2;
import com.example.nut.slotmatchine.fragment.FragmentNumber3;

public class OrderActivity extends AppCompatActivity {

    ActivityOrderBinding binding;
    String getState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order);
        initInstance();

        if (savedInstanceState == null) {

            if (getState.equals("number2")) {
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.from_bottom, R.anim.to_top)
                        .add(R.id.contentContainer, FragmentNumber2.newInstance())
                        .commit();
            } else if (getState.equals("number3")) {
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.from_bottom, R.anim.to_top)
                        .add(R.id.contentContainer, FragmentNumber3.newInstance())
                        .commit();
            }
        }
    }

    private void initInstance() {

        Intent intent = getIntent();
        getState = intent.getStringExtra("number");

        setSupportActionBar(binding.toolbar);
        if (getState.equals("number2")) {
            getSupportActionBar().setTitle("ซื้อเลข 2 ตัว");
        } else if (getState.equals("number3")) {
            getSupportActionBar().setTitle("ซื้อเลข 3 ตัว");
        }
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
