package com.example.nut.slotmatchine.view;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.example.nut.slotmatchine.R;
import com.example.nut.slotmatchine.databinding.ViewGroupNumber2Binding;
import com.example.nut.slotmatchine.databinding.ViewGroupNumber3Binding;
import com.example.nut.slotmatchine.view.state.BundleSavedState;


public class CustomViewGroupNumber2 extends FrameLayout {

    public ViewGroupNumber2Binding binding;
    private EditText lotteryNumber, lotteryValue1, lotteryValue2;

    public CustomViewGroupNumber2(Context context) {
        super(context);
        initInflate();
        initInstances();
    }

    public CustomViewGroupNumber2(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInstances();
        initWithAttrs(attrs, 0, 0);
    }

    public CustomViewGroupNumber2(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInstances();
        initWithAttrs(attrs, defStyleAttr, 0);
    }

    @TargetApi(21)
    public CustomViewGroupNumber2(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initInstances();
        initWithAttrs(attrs, defStyleAttr, defStyleRes);
    }

    private void initInflate() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = ViewGroupNumber2Binding.inflate(inflater, this, true);
    }

    private void initInstances() {
        // findViewById here
//        lotteryNumber = (EditText) findViewById(R.id.lotteryNumber);
//        lotteryValue1 = (EditText) findViewById(R.id.lotteryValue1);
//        lotteryValue2 = (EditText) findViewById(R.id.lotteryValue2);
    }

    private void initWithAttrs(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        /*
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.StyleableName,
                defStyleAttr, defStyleRes);

        try {

        } finally {
            a.recycle();
        }
        */
    }

    public String getLotteryNumber() {
        return binding.lotteryNumber.getText().toString();
    }

    public String getLotteryValue1() {
        return binding.lotteryValue1.getText().toString();
    }

    public String getLotteryValue2() {
        return binding.lotteryValue2.getText().toString();
    }

    public void resetNumber(){
        binding.lotteryNumber.setText("");
        binding.lotteryValue1.setText("");
        binding.lotteryValue2.setText("");
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();

        BundleSavedState savedState = new BundleSavedState(superState);
        // Save Instance State(s) here to the 'savedState.getBundle()'
        // for example,
        // savedState.getBundle().putString("key", value);

        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        BundleSavedState ss = (BundleSavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());

        Bundle bundle = ss.getBundle();
        // Restore State from bundle here
    }

}
